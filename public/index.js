const TRANSLATIONS = {
    en_us: {
        title: "{}'s Pronouns",
        gender: "Gender: {}",
        sexuality: "Sexuality: {}"
    }
};

// URLs can get *very* long if you have many preferred pronouns or a gender/sexuality with a longer name
// To fix this, shorthands exist to shorten the length of your URL without getting rid of your content
const SHORTHANDS = {
    ace: "asexual",
    aro: "aromantic",
    anti: "antisexual",
    pan: "pansexual",
    panr: "panromantic",
    fluid: "genderfluid"
};
const PRONOUN_SHORTHANDS = {
    s: "she/her",
    h: "he/him",
    t: "they/them",
    i: "it/its",
    o: "one/ones"
}

async function copy_link()
{
    const element_copy_link_button = document.getElementById("copy_link_button");
    await navigator.clipboard.writeText(window.location.href);
    element_copy_link_button.innerText = "Copied to clipboard!";
    setTimeout(() => {
        element_copy_link_button.innerText = "Share Pronouns";
    }, 2000);
}

function unshorthand_pro(str, dict) { return !dict.hasOwnProperty(str) ? str : dict[str]; }
function unshorthand(str, dict, split = ' ') { return str.split(split).map(v => unshorthand_pro(v, dict)).join(split); }

function main()
{
    const url_string = window.location.href;
    const url = new URL(url_string);
    const language = "en_us"; // TODO: Add other translations

    const element_title = document.getElementById("title");
    const element_name = document.getElementById("name");
    const element_pronouns = document.getElementById("pronouns");
    const element_gender = document.getElementById("gender");
    const element_sexuality = document.getElementById("sexuality");

    const name = url.searchParams.get("n");
    const pronouns = url.searchParams.get("p")
        .split(",").
        map(val => unshorthand(val.trim(" \t\n\r\f\s"), PRONOUN_SHORTHANDS))
        .join(', ');
    const gender = !url.searchParams.has("g") ? null : unshorthand(url.searchParams.get("g"), SHORTHANDS, ' ');
    const sexuality = !url.searchParams.has("s") ? null : unshorthand(url.searchParams.get("s"), SHORTHANDS, ' ');

    element_title.innerText = TRANSLATIONS[language].title.replace("{}", name);
    element_name.innerText = TRANSLATIONS[language].title.replace("{}", name);
    element_pronouns.innerText = pronouns;
    element_gender.innerText = TRANSLATIONS[language].gender.replace("{}", gender);
    element_sexuality.innerText = TRANSLATIONS[language].sexuality.replace("{}", sexuality);

    if (gender === null || gender === "") document.removeChild(element_gender);
    if (sexuality === null || sexuality === "") document.removeChild(element_sexuality);
}
